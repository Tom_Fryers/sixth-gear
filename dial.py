import math

import pygame
from pygame.locals import *

pygame.init()

class Dial:
    def __init__(self, unit, values, size=250, aa=16, colour=(0, 0, 0), needlecol=(255, 10, 10)):
        self.size = size
        self.colour = colour
        self.needlecol = needlecol
        self.aa = aa

        self.drawsize = self.size * self.aa
        percent = self.drawsize / 100
        self.back = pygame.Surface((self.drawsize, self.drawsize), SRCALPHA)

        self.centre = self.drawsize / 2
        self.radrnd = round(self.centre)

        self.needlelength = 40 * percent
        
        # Draw outer line
        thickness = 3 * percent
        pygame.draw.arc(self.back, colour, (0, 0, self.drawsize, self.drawsize), 0, math.pi * 3 / 2, 0)
        pygame.draw.circle(self.back, colour, (self.radrnd, self.radrnd), self.radrnd)
        pygame.draw.circle(self.back, (0, 0, 0, 0), (self.radrnd, self.radrnd), round(self.centre - thickness))
        pygame.draw.rect(self.back, (0, 0, 0, 0), (self.radrnd, self.radrnd, self.radrnd, self.radrnd))
        
        valrange = (min(values), max(values))
        self.valdiff = valrange[1] - valrange[0]

        # Draw ticks
        ticklength = 5 * percent
        tickthick = 1 * percent
        for value in values:
            angle = self.get_angle(value)
            startpoint = (self.centre + self.centre * -math.sin(angle), self.centre + self.centre * math.cos(angle))
            endpoint = (self.centre + (self.centre - ticklength) * -math.sin(angle), self.centre + (self.centre - ticklength) * math.cos(angle))
            pygame.draw.line(self.back, colour, startpoint, endpoint, round(tickthick))

        pygame.draw.circle(self.back, (0, 0, 0, 0), (self.radrnd, self.radrnd), round(self.centre - ticklength))

        # Draw numbers
        textdist = 10 * percent
        font = pygame.font.Font('data/oxanium/ttf/Oxanium-Regular.ttf', round(5 * percent))
        for value in values:
            angle = self.get_angle(value)
            text = font.render(str(value), 1, colour)
            # Find the correct place to blit the text
            textpoint = (self.centre + (self.centre - textdist) * -math.sin(angle), self.centre + (self.centre - textdist) * math.cos(angle))
            self.back.blit(text, (textpoint[0] - text.get_width() / 2, textpoint[1] - text.get_height() / 2))
        
        # Draw label
        font = pygame.font.Font('data/oxanium/ttf/Oxanium-Regular.ttf', round(7 * percent))
        text = font.render(unit, 1, colour)
        self.back.blit(text, (self.centre - text.get_width() / 2 + 13 * percent, self.centre - text.get_height() / 2 + 42 * percent))
        
        self.im = pygame.transform.smoothscale(self.back, (self.size, self.size))

    def get_angle(self, value):
        return value / self.valdiff * math.pi * 3 / 2

    def draw(self, value):
        image = self.im.copy()
        angle = self.get_angle(value)
        outerpoint = (self.centre + self.needlelength * -math.sin(angle), self.centre + self.needlelength * math.cos(angle))
        pygame.draw.aaline(image, self.needlecol, (self.centre / self.aa, self.centre / self.aa), (outerpoint[0] / self.aa, outerpoint[1] / self.aa))
        return image
    
if __name__ == '__main__':
    SIZE = 250
    test = Dial('mph', range(0, 210, 10), size=SIZE)
    S = pygame.display.set_mode((SIZE, SIZE))
    i = 0
    while True:
        S.fill((0, 0, 0))
        S.blit(test.draw(i), (0, 0))
        pygame.display.update()
        i += 0.01

import math

# Length 4.564

AIRDENSE = 1.225
g = 9.80665

def resistance(speed, front_area, drag_coeff, mass, sand):
    # Calculate rolling_coefficient
    roll_coeff = 0.005 + (1 / 2.07) * (0.01 + 0.0095*((speed / 0.277778) / 100) ** 2)

    if sand:
        roll_coeff *= 10 * speed

    air_res = 0.5 * AIRDENSE * drag_coeff * front_area * speed ** 2
    weight = mass * g
    roll_res = weight * roll_coeff

    if speed > 0:
        return air_res + roll_res
    elif speed < 0:
        return -(air_res + roll_res)
    else:
        return 0

def sigmoid(x):
    return 1 / (1 + math.exp(-x))

def engine_torque(rpm):
    part1 = (1 - sigmoid((3519.4 - rpm) / 10)) * (-1.19521e-5 * rpm ** 2 + 0.075533 * rpm + 533.393)
    part2 = sigmoid((3519.4 - rpm) / 10) * (-1.43925e-5 * rpm ** 2 + 0.153664 * rpm + 288.647)
    torque = part1 + part2
    return torque * 0.87  # ft-lb to Nm conversion, plus decreased, as
                          # the wrong power data were used for the model
def unreal_engine_torque(rotspeed):
    rpm = rotspeed * 60 / (2 * math.pi)
    part1 = sigmoid((3519.4 - rpm) / 10) * engine_torque(rpm)
    part2 = (1 - sigmoid((3519.4 - rpm) / 10)) * (-rpm / 3 + 2550)
    return part1 + part2
    
class Transmission:
    def __init__(self, ticklength):
        self.ticklength = ticklength
        # Engine to axle; x to 1
        self.gears = [2.97, 
                      2.07,
                      1.43,
                      1.00,
                      0.84,
                      0.56,
                     ]
        
        # Axle to wheel; x to 1
        self.axle_ratio = 3.42 * 1.2  # 1.2 for balance reasons
        self.wheel_radius = 0.333
        self.weight = 1414
        self.max_rotspeed = 7000 * 2 * math.pi / 60
        self.revs_per_sec_no_clutch = 500
        self.drag_coeff = 0.29
        self.front_area = 2.072
        self.Crr = 0.0100

        self.gear = 1
        self.speed = 0
        self.rotspeed = 0

    def accelerate(self):
        # Starter motor
        if self.rotspeed < 26:
            self.rotspeed = 26

        torque = unreal_engine_torque(self.rotspeed)

        torque *= self.gears[self.gear - 1]
        torque *= self.axle_ratio

        return torque / self.wheel_radius

    def brake(self):
        return 21000 * 1.5  # Balance
    
    def checkspeed(self, newspeed):
        maxspeed = self.max_rotspeed * (self.wheel_radius) / (self.axle_ratio * self.gears[self.gear - 1])
        self.rotspeed = newspeed / (self.wheel_radius) * self.axle_ratio * self.gears[self.gear - 1]
        if newspeed > maxspeed:
            self.rotspeed = min(newspeed / (self.wheel_radius) * self.axle_ratio * self.gears[self.gear - 1], self.max_rotspeed)
            return False
        return True
    
    def simulate(self, brake=False, accelerate=False, sand=False):
        force = 0
        if brake and self.speed > 0:
            force = -self.brake()

        force -= resistance(self.speed, self.front_area, self.drag_coeff, self.weight, sand=sand)
        if self.checkspeed(self.speed) and accelerate:
            force += self.accelerate()
        accel = force / self.weight
        self.speed = max(0, self.speed + accel * self.ticklength)
        return self.speed

    def shift_up(self):
        if self.gear < 6:
            self.gear += 1
            
    def shift_down(self):
        if self.gear > 1:
            self.gear -= 1

class Car:
    def __init__(self, pos, ticklength, ppm, direction=90):
        self.transmission = Transmission(ticklength)
        self.ticklength = ticklength
        self.ppm = ppm
        self.pos = pos
        self.speed = 0
        self.direction = direction
        self.turnspeed = 50
        self.lastpos = self.pos.copy()
    
    def simulate(self, brake=False, accelerate=False, turn=0, sand=False):
        self.direction += turn * self.ticklength * self.turnspeed * (self.speed ** 0.1)
        self.speed = self.transmission.simulate(brake=brake, accelerate=accelerate, sand=sand)
        self.lastpos = self.pos.copy()
        self.pos[0] += self.ticklength * self.speed * math.cos(math.radians(self.direction)) * self.ppm
        self.pos[1] -= self.ticklength * self.speed * math.sin(math.radians(self.direction)) * self.ppm
    
    def shift_up(self):
        self.transmission.shift_up()

    def shift_down(self):
        self.transmission.shift_down()
    
    def get_x_speed(self):
        return math.cos(math.radians(self.direction)) * self.speed
    
    def get_y_speed(self):
        return -math.sin(math.radians(self.direction)) * self.speed
    
    

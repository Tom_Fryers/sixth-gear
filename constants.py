WIDTH = 1280
HEIGHT = 720

units = {'mph': (3600 / 1609.344, range(0, 210, 10)),
         'km/h': (3.6, range(0, 340, 20)),
         'm/s': (1, range(0, 95, 5)),
        }

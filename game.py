import time
import math

import pygame
from pygame.locals import *

import cars
import dial
import intersect
import ui
import controls

import constants
WIDTH = constants.WIDTH
HEIGHT = constants.HEIGHT

def play_level(S, level, name, highscore, target, startangle, LOADING):
    FPS = 60
    clock = pygame.time.Clock()
    DEADSPEED = 20
    VIEWMOVE = 4

    FONTFG = pygame.font.Font('data/oxanium/ttf/Oxanium-Bold.ttf', 80)
    FONTFT = pygame.font.Font('data/oxanium/ttf/Oxanium-Regular.ttf', 60)

    # Show loading
    BARLENGTH = 800
    S.fill((188, 162, 35))
    S.blit(LOADING, ((WIDTH - LOADING.get_width()) / 2, 400))
    pygame.draw.rect(S, (69, 63, 69), ((WIDTH - BARLENGTH) / 2, 500, BARLENGTH, 10))
    pygame.display.update()
    
    with open('units.txt', 'r') as f:
        unit = f.read()
    
    # Load map
    track = pygame.image.load(f'data/{level}.png').convert()
    track.set_colorkey((255, 0, 255))

    # Find checkpoints
    t = time.time()
    checkpoints = []
    p = pygame.PixelArray(track)

    WHITE = track.map_rgb((255, 255, 255))
    RED = track.map_rgb((255, 0, 0))
    YELLOW = track.map_rgb((255, 255, 0))
    GREEN = track.map_rgb((0, 255, 0))
    CYAN = track.map_rgb((0, 255, 255))
    BLUE = track.map_rgb((0, 0, 255))
    CHECK_COLS = {WHITE: 0, RED: 1, YELLOW: 2, GREEN: 3, CYAN: 4, BLUE: 5}

    mapsize = (track.get_width(), track.get_height())

    oldbarlength = 0
    for x in range(0, mapsize[0], 1):
        for y in range(0, mapsize[1], 1):
            if p[x, y] in CHECK_COLS:
                checkpoints.append((CHECK_COLS[p[x, y]], (x, y)))

        if len(checkpoints) == 12:
            break
        barlength = x / mapsize[0] * BARLENGTH
        if round(barlength) > oldbarlength: 
            pygame.draw.rect(S, (43, 52, 217), ((WIDTH - BARLENGTH) / 2, 500, x / mapsize[0] * BARLENGTH, 10))
            pygame.display.update()
            oldbarlength = round(barlength)
    del p

    checks = [[], [], [], [], [], []]
    for checkpoint in checkpoints:
        checks[checkpoint[0]].append(checkpoint[1])

    leave = False
    
    pygame.mixer.music.load('data/Le Grand Chase.mp3')
    
    while not leave:
        pos = [(checks[0][0][i] + checks[0][1][i]) / 2 for i in range(2)]

        completed = 0
        car = cars.Car(pos, 1/FPS, 7, direction=startangle)
        carimage = pygame.image.load('data/car.png').convert_alpha()
        origsize = carimage.get_width()
        carsize = 35

        speedo = dial.Dial(unit, constants.units[unit][1])
        revo = dial.Dial('rpm', range(0, 7500, 500))

        started = True
        t = -6 * FPS
        ext = False
        turns = [0, 0]
        restart = False
        while not (ext or restart):
            if t == FPS * -4:
                pygame.mixer.music.play(-1)
            t += 1
            keysdown = pygame.key.get_pressed()
            for event in pygame.event.get():
                if event.type == KEYDOWN:
                    if event.key == controls.keys['shift up']:
                        car.shift_up()
                    elif event.key == controls.keys['shift down']:
                        car.shift_down()
                    # Restart
                    elif event.key == K_r:
                        restart = True
                        break
                    
                    if event.key == K_ESCAPE:
                        ext = True
                        break
                    
                elif event.type == QUIT:
                    ext = True
                    break

            accel = keysdown[controls.keys['accelerate']]
            brake = keysdown[controls.keys['brake']]
            
            # Better steering
            if keysdown[controls.keys['left']]:
                if turns[0] < 1:
                    turns[0] += 10 / FPS
            else:
                turns[0] = 0
            
            if keysdown[controls.keys['right']]:
                if turns[1] < 1:
                    turns[1] += 10 / FPS
            else:
                turns[1] = 0
            turn = turns[0] - turns[1]

            sand = True
            if 0 < round(car.pos[0]) < mapsize[0] and 0 < round(car.pos[1]) < mapsize[1]:
                sand = False
                if track.get_at((round(car.pos[0]), round(car.pos[1]))) == (255, 0, 255):
                    sand = True

            if t >= 0:
                car.simulate(accelerate=accel, brake=brake, turn=turn, sand=sand)

            # Determine if the car has passed through checkpoints
            if intersect.intersect(checks[(completed + 1) % 6], [car.pos, car.lastpos]):
                completed += 1
                if completed == 6:
                    break

            # Draw the screen
            S.fill(pygame.Color('#BCA223'))

            S.blit(track, (WIDTH / 2 - car.pos[0] - car.get_x_speed() * VIEWMOVE, HEIGHT / 2 - car.pos[1] - car.get_y_speed() * VIEWMOVE))

            # Rotate then scale the car
            rotim = pygame.transform.rotozoom(carimage, car.direction, carsize / origsize)
            # Draw it
            S.blit(rotim, ((WIDTH - rotim.get_width()) / 2  - car.get_x_speed() * VIEWMOVE, (HEIGHT - rotim.get_height()) / 2  - car.get_y_speed() * VIEWMOVE))
            
            # Draw the gates
            for i, check in enumerate(checks):
                if i > completed:
                    colour = (200, 20, 20)
                else:
                    colour = (20, 200, 20)

                point1 = (check[0][0] + WIDTH / 2 - car.pos[0] - car.get_x_speed() * VIEWMOVE,
                          check[0][1] + HEIGHT / 2 - car.pos[1] - car.get_y_speed() * VIEWMOVE)
                          
                point2 = (check[1][0] + WIDTH / 2 - car.pos[0] - car.get_x_speed() * VIEWMOVE,
                          check[1][1] + HEIGHT / 2 - car.pos[1] - car.get_y_speed() * VIEWMOVE)
                
                if (0 <= point1[0] <= WIDTH and 0 <= point1[1] <= HEIGHT) or (0 <= point2[0] <= WIDTH and 0 <= point2[1] <= HEIGHT):
                    pygame.draw.aaline(S, colour, point1, point2, 0)
            
            # Draw speedometer and revometer
            speed = car.speed * constants.units[unit][0]
            rpm = car.transmission.rotspeed * 60 / 2 / math.pi
            S.blit(speedo.draw(speed), (10, HEIGHT - 260))
            S.blit(revo.draw(rpm), (WIDTH - 260, HEIGHT - 260))

            # Draw gear indicator
            gearim = FONTFG.render(str(car.transmission.gear), 1, (0, 0, 0))
            S.blit(gearim, ((WIDTH - gearim.get_width()) / 2, 600))

            # Draw starting blobs
            if t < 0:
                for i in range(0, 5, 1):
                    if t > -(i + 1) * FPS:
                        pygame.draw.circle(S, (255, 0, 0), (round(WIDTH / 2 + 120 * (2 - i)), 200), 40)

            # Draw timer
            else:
                timetext = FONTFT.render(ui.get_time(t, FPS), 1, (0, 0, 0))
                S.blit(timetext, ((WIDTH - timetext.get_width()) / 2, 50))

            pygame.display.update()
            clock.tick(FPS)

        if not (ext or restart):
            leave = True
            # Show win screen
            pygame.mixer.music.load('data/Take a Chance.mp3')
            pygame.mixer.music.play()
            wintime = ui.get_time(t, FPS)
            wintimeimage = FONTFT.render(wintime + ' / ' + target, 1, (0, 0, 0))
            levelcomp = FONTFG.render(name + ' Complete', 1, (0, 0, 0))

            FONTFN = pygame.font.Font('data/oxanium/ttf/Oxanium-Bold.ttf', 60)
            newbest = FONTFN.render('New high score!', 1, (69, 63, 69))
            beaten = FONTFN.render('Target beaten!', 1, (69, 63, 69))
            
            FONTFW = pygame.font.Font('data/oxanium/ttf/Oxanium-ExtraBold.ttf', 60)
            one = FONTFW.render('1', 1, (69, 63, 69))
            tb = t < ui.from_time(target, FPS)
            nh = t < ui.from_time(highscore, FPS) or highscore == '0:00.00'
            
            if nh:
                with open('scores.csv', 'r') as f:
                    scores = f.read().split('\n')
                    while len(scores) < level:
                        scores.append('')
                    scores[level] = ui.get_time(t, FPS)
                with open('scores.csv', 'w') as f:
                    f.write('\n'.join(scores))
            
            backhome = False
            tp = time.time()
            while not (ext or backhome or restart):
                for event in pygame.event.get():
                    if event.type == QUIT:
                        ext = True
                        leave = True
                        break
                    elif event.type in {MOUSEBUTTONUP, KEYDOWN} and time.time() - tp > 2:
                        backhome = True
                        break

                S.fill(pygame.Color('#BCA223'))
                S.blit(levelcomp, ((WIDTH - levelcomp.get_width()) / 2, 20))
                S.blit(wintimeimage, ((WIDTH - wintimeimage.get_width()) / 2, 500))
                if tb:
                    pygame.draw.circle(S, (255, 215, 0), (round(WIDTH / 2), 400), 50)
                    S.blit(one, ((WIDTH - one.get_width()) / 2, 400 - one.get_height() / 2))
                    S.blit(beaten, ((WIDTH - beaten.get_width()) / 2, 600))
                if nh:
                    S.blit(newbest, ((WIDTH - newbest.get_width()) / 2, 200))
                pygame.display.update()


        elif not restart:
            leave = True

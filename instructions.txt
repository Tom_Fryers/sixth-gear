You have to complete one lap of the track as quickly as you can, passing through several
gates on the way. The gates turn green when you pass through them. You are allowed to
leave the track, but the sand outside the track will slow you down considerably, and if you
miss any of the gates your lap will not be counted.

The controls are:
 • Up = Accelerate
 • Left and Right = Turn
 • Down = Brake
 • W = Change gear up
 • S = Change gear down
 • R = Restart
 • Esc = Return to menu
You can remap these controls easily in controls.py, if you wish.

Tips:
 • Look ahead and brake if there is a tight corner.
 • Quickly change down to first or second gear if you get stuck in sand, or you will stall.
 • The optimal time to shift up is around 6000 RPM.

Click to go back.

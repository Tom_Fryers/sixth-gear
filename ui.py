import pygame
from pygame.locals import *

class Menu:
    def __init__(self, items, font, spacing, colours, topleft):
        self.topleft = topleft
        self.colours = colours
        self.spacing = spacing
        self.items = items
        
        # Render text items
        self.normal = []
        self.hover = []
        self.click = []
        for item in self.items:
            self.normal.append(font.render(item, 1, colours[0]))
            self.hover.append(font.render(item, 1, colours[1]))
            self.click.append(font.render(item, 1, colours[2]))

        self.hovering = None
        
    def get_click(self, mousepos):
        mousepos = (mousepos[0] - self.topleft[0], mousepos[1] - self.topleft[1])
        itempossible = mousepos[1] // self.spacing
        if not 0 <= itempossible < len(self.items):
            self.hovering = None
            return None
        if mousepos[1] % self.spacing > self.normal[itempossible].get_height():
            self.hovering = None
            return None
        
        if mousepos[0] < self.normal[itempossible].get_width():
            self.hovering = itempossible
            return self.items[itempossible]
        
        self.hovering = None
        return None
    
    def draw(self, surface, isclick):
        for item in range(len(self.items)):
            if item == self.hovering:
                if isclick:
                    todraw = self.click[item]
                else:
                    todraw = self.hover[item]
            else:
                todraw = self.normal[item]
            
            surface.blit(todraw, (self.topleft[0], self.spacing * item + self.topleft[1]))

def get_time(time, fps):
    time = time / fps
    if time < 60:
        mins = '0'
    else:
        mins = str(int(time // 60))

    secs = str(round(time % 60, 2))
    if secs[1] == '.':
        secs = '0' + secs
    while len(secs) < 5:
        secs = secs + '0'

    return mins + ':' + secs

def from_time(time, fps):
    time = time.split(':')
    return fps * (int(time[0]) * 60 + float(time[1]))

def text_block(text, font, colour, spacing):
    lines = [font.render(x, 1, colour) for x in text.split('\n')]
    maxwidth = max(x.get_width() for x in lines)
    lastheight = lines[-1].get_height()
    image = pygame.Surface((maxwidth, spacing * len(lines) + lastheight), SRCALPHA)
    for i, line in enumerate(lines):
        image.blit(line, (0, i * spacing))
    
    return image
